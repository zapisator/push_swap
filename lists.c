/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hcummera <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 14:13:00 by hcummera          #+#    #+#             */
/*   Updated: 2019/07/16 14:13:02 by hcummera         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "push_swap.h"
#include <stdlib.h>

ps_list    *ps_lstnew(void const *content, size_t content_size)
{
    struct p_list    *node;
    
    if (!(node = malloc(sizeof(*node))))
        return (NULL);
    if (content && content_size)
    {
        if (!(node->content = malloc(content_size)))
        {
            free(&node);
            return (NULL);
        }
        ft_memset(node->content, '\0', content_size);
        ft_memcpy(node->content, content, content_size);
        node->content_size = content_size;
    }
    else
    {
        node->content = NULL;
        node->content_size = 0;
    }
    node->next = NULL;
    return (node);
}

void    ps_lstadd(ps_list **alst, ps_list *new)
{
    new->next = *alst;
    *alst = new;
}



