/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftothmur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 14:38:42 by ftothmur          #+#    #+#             */
/*   Updated: 2019/07/20 15:16:46 by ftothmur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"


int				validate(int argc, char **argv, t_list **head)
{
	int			fd;
	char		*file;
	ssize_t		size;
	ssize_t		i;

	if ((argc == 0) || ((fd = open(*(argv + 1), O_RDONLY)) == EOF))
	{
		argc == 0 ? ft_putendl("") : ft_putendl("Error");
		return (ERROR);
	}
	file = NULL;
	*head = NULL;
	size = ft_read_file(fd, &file, 1024);
	i = size / 21;
	if (size == EOF || (size < 20 || size > 545 ||
			(i + 1) != (size - i) / 20 || (size - i) % 20) ||
			(tie(head, (size + 1) / 21, file) == EOF))
	{
		ft_putendl("error");
		return (ERROR);
	}
	return ((size + 1) / 21 * 4);
}


int	main(int argc, char **argv)
{

}
